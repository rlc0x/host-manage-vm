---
- name: Create vm {{ item.name }} on hypervisor via terraform 
  block:
    - name: Generate user_data file for {{ item.name }}
      template:
        src: user_data.j2
        dest: "/var/lib/vz/snippets/user_data_{{ item.name }}.yml"
        owner: root
        group: root
        mode: '0644'
      delegate_to: pve
    - name: Create plan file for {{ item.name }}
      template:
        src: plan_tf.j2
        dest: "{{ terraform_base_dir }}/terraform/{{ item.name }}.tf"
        owner: root
        group: root
        mode: '0644'

    - name: Apply the Terraform configuration to the VM
      block:
        - name: Check the status of the Terraform plan file
          vars:
            plan_file: "{{ item.name }}"
          include_tasks: plan_stat.yml
        - name: Terraform apply for {{ item.name }}
          shell:
            cmd: terraform apply --auto-approve --target proxmox_vm_qemu.{{ item.name }}
            chdir: "{{ terraform_base_dir }}/terraform"
          when: plan_file_exists
  when: item.state == "present"

- name: Add vm {{ item.name }} to the inventory file
  lineinfile:
    line: "{{ item.name }} ansible_host={{ item.ip_address }}"
    regex: "^{{ item.name }}.*"
    state: present
    dest: "{{ playbook_dir }}/inventory"
  when: item.state == "present"

- meta: refresh_inventory

- name: Remove the VM created using Terraform
  block:
    - name: Check the status of the Terraform plan file
      vars:
        plan_file: "{{ item.name }}"
      include_tasks: plan_stat.yml
    - name: Destroy the VM {{ item.name }}
      shell:
        cmd: terraform destroy --auto-approve --target proxmox_vm_qemu.{{ item.name }}
        chdir: "{{ terraform_base_dir }}/terraform"
      when: plan_file_exists
    - name: Remove terraform plan file
      file:
        path: "{{ terraform_base_dir }}/terraform/{{ item.name }}.tf"
        state: absent
    - name: Remove terraform user_data from the Proxmox server
      file:
        path: "/var/lib/vz/snippets/user_data_{{ item.name }}.yml"
        state: absent
      delegate_to: pve
    - name: Remove vm {{ item.name }} from the inventory file
      lineinfile:
        line: "{{ item.name }} ansible_host={{ item.ip_address }}"
        regex: "^{{ item.name }}.*"
        state: absent
        dest: "{{ playbook_dir }}/inventory"
  when: item.state == "absent"
