host-manage-vm
=========

Ansible role that generates Terraform plan files and automatically deploys them to a Proxmox server. At this point no clustering is accounted for. 

The work on this role was inspired by the awesome [Terraform ansible demo](https://www.45drives.com/community/articles/automating-proxmox-with-ansible-and-terraform/) presented by [45drives](www.45drives.com) 

This role is meant to be run from an ansible control node. 

After the role is applied the resulting VMs running on the Proxmox server will be managable from the control node. 

Tested With
------------

    - RHEL 8.7
    - Rocky Linux 8.7
    - Proxmox VE 7.3
    - Ansible 2.9
    - Terraform 1.3.9
    - Terraform telemate/proxmox 2.9.11

Requirements
------------

Proxmox-ve server with a [User and Token](https://registry.terraform.io/providers/Telmate/proxmox/latest/docs) that allows creating virtual machines.

Role Variables
--------------

Available variables are listed below, look in `defaults/main.yml` for additional variables

    _domain: example.com
	pve_node: pve 
	vm_config:
	  memory: 1024
	  cores: 1
	  disk_size: 20G
	  disk_storage: ZPool1
	  username: ansible
	  password: "{{ vault_ansible_password }}"
	  domain_name: "{{ _domain }}"
	  dns: 192.168.1.12
	  image: rocky8.7-ci
	pve_credentials:
	  api_endpoint: "{{ vault_api_endpoint }}"
	  api_token_id: "{{ vault_api_token_id }}"
	  api_secret: "{{ vault_api_secret }}"
	  api_password: "{{ vault_api_password }}"
	nodes:
	  - name: control
	    comments: "Ansible node0"
	    ip_gateway: 192.168.1.1
	    ip_address: 192.168.1.40
	    ip_cidr: 24
	    dns: 192.168.1.152
	    fqdn: node0.example.com
	    second_disk: False
	    cdrom: True
	    cores: 1
	    state: absent
	  - name: node1
	    comments: "Ansible node1"
	    ip_gateway: 192.168.1.1
	    ip_address: 192.168.1.41
	    ip_cidr: 24
	    dns: 192.168.1.152
	    fqdn: node1.example.com
	    second_disk: True
	    cdrom: False
	    state: absent
	  - name: node2
	    comments: "Ansible node2"
	    ip_gateway: 192.168.1.1
	    ip_address: 192.168.1.42
	    ip_cidr: 24
	    dns: 192.168.1.152
	    fqdn: node2.example.com
	    second_disk: False
	    cdrom: False
	    state: absent

Dependencies
------------


Example Playbook
----------------

```yaml
- name: Manage VMs on Proxmox
  hosts: localhost
  gather_facts: False
  any_errors_fatal: True
  become: True
  vars:
    _domain: example.com
	pve_node: pve 
	vm_config:
	  memory: 1024
	  cores: 1
	  disk_size: 20G
	  disk_storage: ZPool1
	  username: ansible
	  password: "{{ vault_ansible_password }}"
	  domain_name: "{{ _domain }}"
	  dns: 192.168.1.12
	  image: rocky8.7-ci
	pve_credentials:
	  api_endpoint: "{{ vault_api_endpoint }}"
	  api_token_id: "{{ vault_api_token_id }}"
	  api_secret: "{{ vault_api_secret }}"
	  api_password: "{{ vault_api_password }}"
	nodes:
	  - name: node0
	    comments: "Ansible node0"
	    ip_gateway: 192.168.1.1
	    ip_address: 192.168.1.40
	    ip_cidr: 24
	    dns: 192.168.1.152
	    fqdn: node0.example.com
	    second_disk: False
	    cdrom: True
	    cores: 1
	    state: present
	  - name: node1
	    comments: "Ansible node1"
	    ip_gateway: 192.168.1.1
	    ip_address: 192.168.1.41
	    ip_cidr: 24
	    dns: 192.168.1.152
	    fqdn: node1.example.com
	    second_disk: True
	    cdrom: False
	    state: present
	  - name: node2
	    comments: "Ansible node2"
	    ip_gateway: 192.168.1.1
	    ip_address: 192.168.1.42
	    ip_cidr: 24
	    dns: 192.168.1.152
	    fqdn: node2.example.com
	    second_disk: False
	    cdrom: False
	    state: present
    roles:
      - host-manage-vm
```

License
-------

MIT

Author Information
------------------

Raymond Cox
